# EEGLAB plugin to visualize cEEGrid data using fieldtrip functions

Scripts and tools for working with the cEEGrid EEG electrode setup. 

### What are cEEGrids? 
The cEEGrid is an electrode array for measuring EEG scalp potentials around the ear. It was developed in the Neuropschology Lab in the Psychology Department of the Carl von Ossietzky University Oldenburg, Germany.

More details on how to use it, can be found here: http://ceegrid.com/home/

### What does this plugin do? 
The plugin uses [fieldtrip](http://www.fieldtriptoolbox.org/) functions to plot cEEGrid data in the correct 
channel arrangement or as topoplots in [EEGlab](https://sccn.ucsd.edu/eeglab/index.php). 

### Requirements
In addition to the requirements of EEGLAB, cEEGrid plug-in needs some fieldtrip functions to work. Using the EEGLAB extension manager you can add Fieldtrip-lite to have all necessary functions in your path.  
For plotting the ICA topographies and components it is necessary that you have run ICA. 

### Download and Installation
 1) Download the compressed cEEGrid plug-in file (*.zip) into your 'plugins' directory of your  EEGLAB distribution.

2) Uncompress the donwloaded file using utility software. Inside your 'plugins' directory, you should now have a directory called 'cEEGrid_EEGLAB_plugin' 
containing all the necessary m-files . Starting EEGLAB should now automatically recognise and add the plug-in. 
You should see the following line appear in your matlab environment window:

EEGLAB: adding "cEEGrid_EEGLAB_Plugin" v.0.4 (see >> help eegplugin_ceegrid)

That's it.

### Channel Labeling Convention
The plugin assumes the following channel labeling convention. 
R4a and R4b are generally used as ground and reference electrode, respectively. This will depend on your amplifier and connector. 

<img src="/images/cEEGridLabel.png" width="80%">


# Plotting

## Time domain (cEEGrid Channel ERP)
The time domain signal for each channel can be plotted with "cEEGrid channel ERP".

In the pop up you can specify the channels to plot using the channel names. Otherwise all channels will be shown. 

![cEEGrid Layout](/images/multiplotER.png)

The resulting figure.

![cEEGrid Layout](/images/multiplotER_Result.PNG)

The channels that are not in your .set file (here R04a and R04b) won't show in your plot. 

As we are using fieldtrip functions for plotting we have additional functionality that is normaly not part of EEGLAB

For example, you can select a couple of electrodes with the mouse, and left click on the selection. 

![channel Selection](/images/selectChannels.PNG)

A new figure will open up, showing you the mean signal of all selected channels.


![mean Channel](/images/meanChannel.png)

In the resulting figure you can select a specific time window with the mouse. 

![mean Channel selection](/images/meanChannelSelection.png)

A new figure will open up, showing the topography for the selected time window. 

![selected Topography](/images/selectedTopography.PNG)

Note that the topography looks slightly different from the topography that results from 'cEEGrid topography' due to differences in the default settings. 



## Topoplots (cEEGrid Topography)
Topographies for specific time point (if one value is given) or time window (if two values are given). 

In the pop up you can specify the channels to plot using the channel names. Otherwise all channels will be shown. 
You have to indicate the time point or time window for which the topoplot should be made. 

You can choose to either show both grids, or only the left or the right grid. 


![cEEGrid Layout](/images/topoplotER.png)


The resulting figure.

![cEEGrid Layout](/images/topoplotER_Result.png)

## ICA topographies (cEEGrid ICA topography)

After you have computed ICA components you can plot them using "cEEGrid ICA topography".
The resulting figure.

![cEEGrid Layout](/images/ICAcomponents.PNG)

## ICA topographies (cEEGrid ICA components)

After you have computed ICA components you can plot them using "cEEGrid ICA components".
The resulting figures. For each component a window is opened showing the component topography and the corresponding time course. 

![cEEGrid Layout](/images/ICAcomponents_timecourse.PNG)


## Different Electrode layouts (fEEGrid) 
Currently, you can choose (for 'cEEGrid channel' ERP and 'cEEGrid topography') between cEEGrids and fEEGrids.

![fEEGrid Result](/images/topoplotER_Result_fEEGrid.png)

# Study displays
This plugin allows to disply your data within the cEEGrid and fEEGrid layouts at the study level (with different groups and conditions to compare). 

The first step that you have to do is to create a usual EEGLAB STUDY with your datasets, including a usual study design with the different conditions labeled. 
Then you should precompute the ERP and spectral activities to be plotted. After that, you can click wihin the STUDY tool EEGLAB menu, and you can select the "Plotting cEEGrid" button.

A pop-up window appears when you can select to plot either time-domain data ("ERP") or frequency-domain data ("Spec"). Likewise, you can either plot the tme-course/frequency variations ("Plot channels"), or the topographies ("Plot topo").

The ERP and spectral data are computed the same way as previously explained. For channel plots all the conditions are displayed on single plots. For topographies, there is one pop-up window  per condition (with the name of the condition displayed on the figure header).

You can plot channels and topographies consecultively, alike you can plot time and frequency-domain data consecutively.

The resulting figure.

![Study plotting](/images/Study_timecourse.PNG)

## Publications 
Debener, S., Emkes, R., De Vos, M. & Bleichner, M. Unobtrusive ambulatory EEG using a smartphone and flexible printed electrodes around the ear. Sci. Rep. 5, 16743 (2015).

Mirkovic, B., Bleichner, M. G., De Vos, M. & Debener, S. Target speaker detection with concealed EEG around the ear. Front. Neurosci. 10, (2016).

Bleichner, M. G., Lundbeck, M., Selisky, M., Minow, F., Jager, M., Emkes, R., Debener, S. & De Vos, M. Exploring miniaturized EEG electrodes for brain-computer interfaces. An EEG you do not see? Physiol. Rep. 3, e12362–e12362 (2015).

Bleichner, M.G., Mirkovic, B. & Debener, S. Identifying auditory attention with ear-EEG: cEEGrid versus high-density cap-EEG comparison. J. Neural Eng. 13, (2016).

Bleichner, M.G. & Debener, S. Concealed, unobtrusive ear-centered EEG acquisition: cEEGrids for transparent EEG. Front Hum Neurosci. 11, (2017).

Pacharra, N., Debener, S. & Wascher, E. Concealed Around-the-Ear EEG Captures Cognitive Processing in a Visual Simon Task. Front. Hum. Neurosci. 11, (2017).

Blum, Sarah & Debener, Stefan & Emkes, Reiner & Volkening, Nils & Fudickar, Sebastian & G. Bleichner, Martin. (2017). EEG Recording and Online Signal Processing on Android: A Multiapp Framework for Brain-Computer Interfaces on Smartphone. BioMed Research International. 2017. 1-22. 10.1155/2017/3072870. 

Denk, F., Grzybowski, M.,  Ernst, S. M. A., Kollmeier B., Debener, S., Bleichner, M. G. Event-related potentials measured from in and around the ear electrodes integrated in a live hearing device for monitoring sound perception, Trends in Hearing 22 (2018)
