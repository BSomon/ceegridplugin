% pop_cEEGrid_icatopoplot(INEEG, nargin); - uses fieldtrip functions to plot data in the cEEGrid shape.
%
% Usage:
% >> OUTEEG =pop_cEEGrid_icatopoplot(INEEG, nargin);
%
% Inputs:
%   INEEG         - input EEG dataset
%   AMP           - your recording setup
%
% Outputs:
%   OUTEEG        - output dataset
%
% See also:
%    EEGLAB
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG, com] = pop_cEEGrid_icatopoplot(EEG, layoutname);

com = ''; % this initialization ensure that the function will return something

if nargin < 1
    help pop_cEEGrid_icatopoplot;
    return;
end;

% get all layouts from the layouts directory
scriptName = mfilename('fullpath');
[currentpath, filename, fileextension]= fileparts(scriptName);
layouts=dir([currentpath filesep 'Layouts',filesep '*.mat']);
layoutoptions=['']
for k=1:numel(layouts)
    layoutoptions=strcat(layoutoptions,layouts(k).name(1:end-4));
    if k<numel(layouts)
        layoutoptions=strcat(layoutoptions,' | ');
    end
end

if nargin < 2
    
    guititle='cEEGrid ICA topographies';
    geom={1 };
    uilist={{ 'style', 'text', 'string', 'Plot topographies' }};
    
    result=inputgui('geometry', geom , 'uilist', uilist);
    %uses both sides by default
    cfg.side = layouts(1).name(1:end-4); % get the layout file
    
else
    cfg.side = layoutname;
end


cfg.channel={'all' };
cfg.interplimits ='head';
cfg.interpolation='nearest';
cfg.plotSort='ft_topoplotER';
cfg.newfig='no';
dimVec=1;
figure;
for k=1:EEG.nbchan
    subplot(4,5,k);
    Data=EEG.icawinv(:,k);
    DataLabels={EEG.chanlocs.labels};
    ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg)
end

% for each component the time course and the respective topography
% are shown

%ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg)

% return the string command
% -------------------------
com = sprintf('pop_cEEGrid_icatopoplot( %s, %s);', inputname(1), vararg2str({cfg.side}));


end