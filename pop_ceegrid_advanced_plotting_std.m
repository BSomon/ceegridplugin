
% pop_ceegrid_advanced_plotting_std(nargin); - uses fieldtrip functions to plot data in the cEEGrid shape.
%
% Usage:
% >> [STUDY, ALLEEG, com] =pop_ceegrid_basic_plotting(nargin);
%
% Inputs:
%   nargin        - The first argument is the STUDY structure containing
%					the dataset information, study designs etc.
%				  - The second argument is the ALLEEG structure containing
%					the single datasets
%
% Outputs:
%   STUDY		  - output STUDY structure
%	ALLEEG		  - output ALLEEG structure
%	com			  - history of com
%
% See also:
%    pop_ceegrid_advanced_plotting(); pop_ceegrid_multiplot();
%    pop_ceegrid_topoplot(); ceegrid_advanced_plotting()
%
% Author: Bertille Somon, Artificial and Natural Intelligence Toulouse Institute - ISAE Supaero, Université de Toulouse, France 2020
% Copyright (C) 2019 Martin Bleichner
%
% This function allows to plot the cEEGrid data at the study level in the
% time domain (ERP) and frequency domain (SPEC) through the FieldTrip
% functions (topoplotER and multiplotER) in the cEEGrid layout.
%
%
% Usage:
%	>> [STUDY, ALLEEG] = pop_ceegrid_advanced_plotting_std(STUDY, ALLEEG);
%	% Opens the cEEGrid plotting pop-up
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [STUDY, ALLEEG, com] = pop_ceegrid_advanced_plotting_std(varargin)


com = ''; % this initialization ensure that the function will return something


if nargin < 1
	help pop_ceegrid_advanced _plotting_std;
	return;
end
% if the user press the cancel button

% check for required fieldtrip functions
if ~plugin_status('fieldtrip-lite')
	display('The fieldtrip-lite plugin is not installed. Please install the fieldtrip-lite plugin manually.');
	% this does not work
	%plugin_askinstall('fieldtrip-lite', [], true);
end
% plotSort= varargin{1};
% datatype = varargin{2}; %whether it is ERP or Spec
% get all layouts from the layouts directory
scriptName = mfilename('fullpath');
[currentpath, filename, fileextension]= fileparts(scriptName);
layouts=dir([currentpath filesep 'Layouts',filesep '*.mat']);
layoutoptions=[''];
for k=1:numel(layouts)
	layoutoptions=strcat(layoutoptions,layouts(k).name(1:end-4));
	if k<numel(layouts)
		layoutoptions=strcat(layoutoptions,' | ');
	end
end
if ~ischar(varargin{1})
	if nargin < 2
		error('pop_chanplot(): You must provide ALLEEG and STUDY structures');
	end
	STUDY  = varargin{1};
	STUDY.tmphist = '';
	ALLEEG = varargin{2};
	fig_arg{1}{1} = STUDY;
	fig_arg{1}{2} = ALLEEG;
	
	guititle = 'cEEGrid plotting for studies';
	geom = { [1 1] [1 1]};
	% The ICA functionality doesn't work at the study level yet
	uilist   = { ...
		{'style' 'checkbox'   'enable'   'on' 'string' 'ERP'	    'Callback' 'pop_ceegrid_advanced_plotting_std(''erp'',gcf);'} ...
		{'style' 'checkbox'   'enable'   'on' 'string' 'Spec'		'Callback' 'pop_ceegrid_advanced_plotting_std(''spec'',gcf);'} ...
		%		{'style' 'checkbox'	  'enable'   'on' 'string' 'ICA'		    'Callback' 'pop_ceegrid_advanced_plotting_std(''ica'', gcf)'} ...
		{'style' 'pushbutton' 'enable'   'on' 'string' 'Plot chanels' 'Callback' 'pop_ceegrid_advanced_plotting_std(''multiplotER'',gcf);'} ...
		{'style' 'pushbutton' 'enable'   'on' 'string' 'Plot topo'    'Callback' 'pop_ceegrid_advanced_plotting_std(''topoplotER'',gcf);'}  ...
		};
	
	[result, userdat]=inputgui( 'geometry', geom , 'uilist', uilist, 'helpcom',...
		'pophelp(''pop_ceegrid_advanced_plotting_std'')', 'title', guititle,...
		'userdata', fig_arg);
	
	% history update
	% -------
	if ~isempty(userdat)
		STUDY = userdat{1}{1};
	end
	com = STUDY.tmphist;
	STUDY = rmfield(STUDY, 'tmphist');
	
	
else
	hdl = varargin{2};  %figure handle
	userdat  = get(varargin{2}, 'userdat');
	STUDY   = userdat{1}{1};
	ALLEEG    = userdat{1}{2};
 	try
		switch varargin{1}
			case {'erp', 'spec'}
				%Read and average the ERP data at the study level
				% erpdata = [cell array] size nCond x nGroup
				%			each cell contains a matrix of size nTimes x nChan x nSubj
				[STUDY, erpdata, erptimes] = std_readdata(STUDY, ALLEEG, 'datatype', varargin{1},...
					'channels', {STUDY.changrp(:).name});
				% Average across subjects and transpose for FieldTrip:
				% erpdata = [cell array] nCond x nGroup
				%			each cell contains nChan x nTimes
				% Concatenate conditions in one matrix
				erpdata = cellfun(@(x)mean(x,3)', erpdata, 'UniformOutput', false);
				ERP = cat(3, erpdata{1:end});
				
				%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				Conditions = cell(1, length(STUDY.design(STUDY.currentdesign).variable));
				for iCond = 1:length(STUDY.design(STUDY.currentdesign).variable)  %Gives the number of variables
					Conditions{iCond} = STUDY.design(STUDY.currentdesign).variable(iCond).value;
					if strcmpi(STUDY.design(STUDY.currentdesign).variable(iCond).label, 'group')
						ncols = [iCond size(Conditions{iCond},2)]; end
					if strcmpi(STUDY.design(STUDY.currentdesign).variable(iCond).label, 'condition')
						nlines = [iCond size(Conditions{iCond},2)]; end
					idxCell = find(double(cellfun(@iscell, Conditions{iCond})));
					if ~isempty(idxCell)
						Conditions{iCond}{idxCell} = cell2mat(Conditions{iCond}{idxCell});
					end
				end
				if length(STUDY.design(STUDY.currentdesign).variable)==1
					ceCond = Conditions{1};
				else
					for iCol = 1:ncols(2)
						for iLine = 1:nlines(2)
							sCond = ncols(2)*(iLine-1)+iCol;
							ceCond{sCond} = [Conditions{ncols(1)}{iCol}, '_', Conditions{nlines(1)}{iLine}];
						end
					end
				end
				userdat{1}{3} = ERP;
				userdat{1}{4} = erptimes;
				userdat{1}{5} = ceCond;
				userdat{1}{6} = varargin{1};
				set(hdl, 'userdat', userdat);
			case 'multiplotER'
				hdl = varargin{2};  %figure handle
				userdat  = get(varargin{2}, 'userdat');
				STUDY   = userdat{1}{1};
				ALLEEG    = userdat{1}{2};
				ERP = userdat{1}{3};
				erptimes = userdat{1}{4};
				Cond = userdat{1}{5};
				datatype = userdat{1}{6};
				guititle='cEEGrid multiplotER -- pop_ceegrid_advanced_plotting';
				geom={[1 1] [1 1] [1 1]  [1] [1]};
				
				
				editoptions=[''];
				editwhichchannels = [''];
				
				uilist={
					{ 'style', 'text', 'string', 'Channels subset([]=all) ' },...
					{ 'style', 'edit', 'string', editwhichchannels },...
					{ 'style'   'text'     'string'    'Plot title' } ...
					{ 'style'   'edit'     'string'    fastif(~isempty(STUDY.name), [STUDY.name], '') } ...
					{ 'style', 'text', 'string', 'Select Grid:' },...
					{ 'style', 'listbox', 'string', layoutoptions},...
					{ 'style'   'text'     'string'    [ '-> Additional plot options (see Help)' ] } ...
					{ 'style'   'edit'     'string'  editoptions }};
				
				result=inputgui( geom ,  uilist, 'pophelp(''pop_ceegrid_advanced_plotting'')', guititle, [], 'normal');
				
				cfg = [];
				dimVec = erptimes;
				Data{1} = ERP;
				Data{2} = Cond;
				%use either all channels or the ones specified
				if isempty(result{1})
					DataLabels={STUDY.changrp(:).name};
				else
					DataLabels=strsplit(result{1});
				end
				% set layout file
				cfg.side = layouts((result{3})).name(1:end-4); % get the layout file
				
				ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg);
				
				% update Study history
				a = ['[STUDY, ALLEEG] = pop_ceegrid_advanced_plotting_std(STUDY, ALLEEG, '''...
					varargin{1},''', ''' datatype ''');'];
				STUDY.tmphist =  sprintf('%s\n%s',  STUDY.tmphist, a);
				userdat{1}{1} = STUDY;
				set(hdl, 'userdat', userdat);
			case 'topoplotER'
				hdl = varargin{2};  %figure handle
				userdat  = get(varargin{2}, 'userdat');
				STUDY   = userdat{1}{1};
				ALLEEG    = userdat{1}{2};
				ERP = userdat{1}{3};
				erptimes = userdat{1}{4};
				Cond = userdat{1}{5};
				datatype = userdat{1}{6};
				
				guititle='cEEGrid topoplotER -- pop_ceegrid_advanced_plotting';
				geom={[1.5 1]  [1.5 1] 1 [1.5 1] [1 1 ] [1] [1]};
				
				txtwhat2plot1 = 'Plotting ERP scalp maps at these latencies';
				txtwhat2plot2 = sprintf('(range: %d to %d ms, two values -> mean over time range):', ...
					round(ALLEEG(1).xmin*1000), round(ALLEEG(1).xmax*1000));
				editwhat2plot = [''];
				editwhichchannels = [''];
				editoptions=[''];
				uilist={{ 'style', 'text', 'string', 'Channels subset([]=all)' },...
					{ 'style', 'edit', 'string', editwhichchannels } ,...
					{'style', 'text', 'string', txtwhat2plot1},...
					{ 'style', 'edit', 'string', editwhat2plot},...
					{'style', 'text', 'string', txtwhat2plot2},...
					{ 'style'   'text'     'string'    'Plot title' } ...
					{ 'style'   'edit'     'string'    fastif(~isempty(STUDY.name), [STUDY.name], '') } ...
					{ 'style', 'text', 'string', 'Select Grid:' },...
					{ 'style', 'listbox', 'string', layoutoptions},...
					{ 'style'   'text'     'string'    [ '-> Additional plot options (see Help)' ] } ...
					{ 'style'   'edit'     'string'  editoptions },...
					};
				
				result=inputgui( geom ,  uilist, 'pophelp(''pop_ceegrid_advanced_plotting'')', guititle, [], 'normal');
				
				cfg=[];
				cfg.channel={'all'};
				cfg.interplimits ='head';
				cfg.interpolation='nearest';
				cfg.plotSort='ft_topoplotER';
				cfg.side = layouts((result{4})).name(1:end-4); % get the layout file
				
				if isempty(result{2})
					cindex = [1 length(erptimes)];
				else
					[~,cindex,~] = closest(erptimes, str2num(result{2}));
				end
				dimVec = 1;
				
				if numel(cindex)==1
					Data{1}=mean(ERP(:,cindex(1):cindex(1),:),2);
					Data{2} = Cond;
				else
					Data{1}=mean(ERP(:,cindex(1):cindex(2),:),2);
					Data{2} = Cond;
				end
				
				if isempty(result{1})
					DataLabels={STUDY.changrp(:).name};
				else
					DataLabels=strsplit(result{1});
				end
				
				ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg);
				
				%History update
				a = ['[STUDY, ALLEEG] = pop_ceegrid_advanced_plotting_std(STUDY, ALLEEG, '''...
					varargin{1},''', ''', datatype ''');'];
				STUDY.tmphist =  sprintf('%s\n%s',  STUDY.tmphist, a);
				userdat{1}{1} = STUDY;
				set(hdl, 'userdat', userdat);
			otherwise
				display('Unknown plotting option.');
				return;
		end
 	catch
 		eeglab_error;
 	end
	
end
end
