% pop_ceegrid_basic_plotting(INEEG, nargin); - uses fieldtrip functions to plot data in the cEEGrid shape.
%
% Usage:
% >> OUTEEG =pop_ceegrid_basic_plotting(INEEG, nargin);
%
% Inputs:
%   INEEG         - input EEG dataset
%   AMP           - your recording setup
%
% Outputs:
%   OUTEEG        - output dataset
%
% See also:
%    EEGLAB
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG, com] = pop_ceegrid_advanced_plotting(EEG, varargin);


com = ''; % this initialization ensure that the function will return something

if nargin < 1
	help pop_ceegrid_advanced _plotting;
	return;
end
% if the user press the cancel button

% check for required fieldtrip functions
if ~plugin_status('fieldtrip-lite')
    display('The fieldtrip-lite plugin is not installed. Please install the fieldtrip-lite plugin manually.');
    % this does not work
    %plugin_askinstall('fieldtrip-lite', [], true);
end
plotSort= varargin{1};

% get all layouts from the layouts directory
scriptName = mfilename('fullpath');
[currentpath, filename, fileextension]= fileparts(scriptName);
layouts=dir([currentpath filesep 'Layouts',filesep '*.mat']);
layoutoptions=[''];
for k=1:numel(layouts)
    layoutoptions=strcat(layoutoptions,layouts(k).name(1:end-4));
    if k<numel(layouts)
        layoutoptions=strcat(layoutoptions,' | ');
    end
end



switch plotSort
    case 'multiplotER'
        guititle='cEEGrid multiplotER -- pop_ceegrid_advanced_plotting';
        geom={[1 1] [1 1] [1 1]  [1] [1]};
        
        
        editoptions=[''];
        editwhichchannels = [''];
        
        uilist={
            { 'style', 'text', 'string', 'Channels subset([]=all) ' },...
            { 'style', 'edit', 'string', editwhichchannels },...
            { 'style'   'text'     'string'    'Plot title' } ...
            { 'style'   'edit'     'string'    fastif(~isempty(EEG.setname), [EEG.setname], '') } ...
            { 'style', 'text', 'string', 'Select Grid:' },...
            { 'style', 'listbox', 'string', layoutoptions},...
            { 'style'   'text'     'string'    [ '-> Additional plot options (see Help)' ] } ...
            { 'style'   'edit'     'string'  editoptions }};
        
        result=inputgui( geom ,  uilist, 'pophelp(''pop_ceegrid_advanced_plotting'')', guititle, [], 'normal');
        
    case 'topoplotER'
        guititle='cEEGrid topoplotER -- pop_ceegrid_advanced_plotting';
        geom={[1.5 1]  [1.5 1] 1 [1.5 1] [1 1 ] [1] [1]};
        
        txtwhat2plot1 = 'Plotting ERP scalp maps at these latencies';
        txtwhat2plot2 = sprintf('(range: %d to %d ms, two values -> mean over time range):', ...
            round(EEG.xmin*1000), round(EEG.xmax*1000));
        editwhat2plot = [''];
        editwhichchannels = [''];
        editoptions=[''];
        uilist={{ 'style', 'text', 'string', 'Channels subset([]=all)' },...
            { 'style', 'edit', 'string', editwhichchannels } ,...
            {'style', 'text', 'string', txtwhat2plot1},...
            { 'style', 'edit', 'string', editwhat2plot},...
            {'style', 'text', 'string', txtwhat2plot2},...
            { 'style'   'text'     'string'    'Plot title' } ...
            { 'style'   'edit'     'string'    fastif(~isempty(EEG.setname), [EEG.setname], '') } ...
            { 'style', 'text', 'string', 'Select Grid:' },...
            { 'style', 'listbox', 'string', layoutoptions},...
            { 'style'   'text'     'string'    [ '-> Additional plot options (see Help)' ] } ...
            { 'style'   'edit'     'string'  editoptions },...
            };
        
        result=inputgui( geom ,  uilist, 'pophelp(''pop_ceegrid_advanced_plotting'')', guititle, [], 'normal');
        
    case 'ICA topography'
        guititle='cEEGrid ICA topographies';
        geom={1 };
        uilist={{ 'style', 'text', 'string', 'Plot topographies' }};
        
        result=inputgui('geometry', geom , 'uilist', uilist);
        
    case 'ICA components'
        guititle='cEEGrid ICA components';
        geom={1 };
        uilist={{ 'style', 'text', 'string', 'Plot topographies and time courses' }};
        result=inputgui('geometry', geom , 'uilist', uilist);
    otherwise
        display('Unknown plotting option.');
        return;
        
end



[Data,dimVec,DataLabels,cfg]=prepare_plotcall(plotSort,result);
ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg);

end
