% Davis, CA, March, 2011
function [cvalue, cindex, cdiff] = closest(data, target)
error(nargchk(1,2,nargin))
error(nargoutchk(0,3,nargout))
if nargin<2; target = [];end
ntarget = length(target);
[cvalue, cindex, cdiff]   = deal(zeros(1, ntarget));
for i=1:ntarget
      if isnan(target(i)) || isinf(target(i))
            [cvalue(i), cindex(i), cdiff(i)] = deal(NaN);
      else
            [cdiff(i), cindex(i)] = min(abs(data-target(i)));
            cvalue(i) = data(cindex(i));
      end
end