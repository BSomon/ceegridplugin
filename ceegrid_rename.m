% ceegrid_rename() - adds the correct labels for your cEEGrid recording 
%		         depending on your connector layout. This is very specific to your individual hardware. 
%                For all plotting we assume that the channels are name a L01 to L08 (inclduing L04a and L04b )
%
% Usage:
% >> OUTEEG =pop_ceegrid_rename(INEEG, AMP);
% 
% Inputs: 
%   INEEG     	  - input EEG dataset
%   AMP           - your recording setup 
%
% Outputs: 
%   OUTEEG        - output dataset
%
% See also:
%    EEGLAB
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


function [ EEG ] = correct_channel_names_ceegrid1( EEG,AMP )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
% martin bleichner 09072015
%           - now it can handle both ceeGrid amps, and labels the channels
%           according to Stefans terminology. it only works with either 27
%           or 24 channels


lower(AMP)
switch lower(AMP)
    
    case 'smarting v27'
    display('Two grids. Amp light facing experimenter. Pins: Bottom right.')
     if EEG.nbchan==27
      channelnames={{1,'R05'},{2,'R03'},{3,'R01'}, {4,'Second Pin (fl)'},{5,'Third Pin (fl)'},...
              {6,'x3'},{7,'x4'},{8,'Forth Pin (fl)'}, {9,'First Pin (fl)'},{10,'R02'},{11,'R04'},...
              {12,'R06'},{13,'R08'},{14,'R07'}, {15,'L07'},{16,'L05'},{17,'L04a '},...
              {18,'L03'},{19,'L01'},{20,'L02'}, {21,'L04'},{22,'L04b'},{23,'L06'},...
              {24,'L08'},{25,'Yaw'},{26,'Pitch'}, {27,'Roll'}};
     elseif  EEG.nbchan==24    
          channelnames={{1,'R05'},{2,'R03'},{3,'R01'}, {4,'Second Pin (fl)'},{5,'Third Pin (fl)'},...
              {6,'x3'},{7,'x4'},{8,'Forth Pin (fl)'}, {9,'First Pin (fl)'},{10,'R02'},{11,'R04'},...
              {12,'R06'},{13,'R08'},{14,'R07'}, {15,'L07'},{16,'L05'},{17,'L04A DRL Hom'},...
              {18,'L03'},{19,'L01'},{20,'L02'}, {21,'L04'},{22,'L04B REF Hom'},{23,'L06'},...
              {24,'L08'}};
     elseif EEG.nbchan==30
               channelnames={{1,'R05'},{2,'R03'},{3,'R01'}, {4,'Second Pin (fl)'},{5,'Third Pin (fl)'},...
              {6,'x3'},{7,'x4'},{8,'Forth Pin (fl)'}, {9,'First Pin (fl)'},{10,'R02'},{11,'R04'},...
              {12,'R06'},{13,'R08'},{14,'R07'}, {15,'L07'},{16,'L05'},{17,'L04a'},...
              {18,'L03'},{19,'L01'},{20,'L02'}, {21,'L04'},{22,'L04b'},{23,'L06'},...
              {24,'L08'},{25,'Yaw'},{26,'Pitch'}, {27,'Roll'},{28,'AccelerX'},{29,'AccelerY'}, {30,'AccelerZ'} };
     else
         msg = ['Error occurred. There are ' num2str(EEG.nbchan) ' channels in your data. I cannot deal with this, you should have either 24 or 27 channels. As you do not you either do not know what you are doing or you know it exactly what you are doing. In the first case: Get some help. In the second case: deal with it, I cannot help you.' ];
         error(msg)
     end
          
     
         case 'smarting v35'
    display('Two grids. Amp light facing experimenter. Pins: Bottom right.')
     if EEG.nbchan==27
      channelnames={{1,'R08'},{2,'R07'},{3,'R06'}, {4,'R05'},{5,'R04'},...
              {6,'R03'},{7,'R02'},{8,'R01'}, {9,'L08'},{10,'L07'},{11,'L06'},...
              {12,'L05'},{13,'L04b'},{14,'L04a'}, {15,'L04'},{16,'L03'},{17,'L02'},...
              {18,'L01'},{19,'Pin 1'},{20,'Pin 2'}, {21,'Pin 3'},{22,'Pin 4'},{23,'Pin 5'},...
              {24,'Pin 6'},{25,'Yaw'},{26,'Pitch'}, {27,'Roll'}};
     elseif  EEG.nbchan==24    
              channelnames={{1,'R08'},{2,'R07'},{3,'R06'}, {4,'R05'},{5,'R04'},...
              {6,'R03'},{7,'R02'},{8,'R01'}, {9,'L08'},{10,'L07'},{11,'L06'},...
              {12,'L05'},{13,'L04b'},{14,'L04a'}, {15,'L04'},{16,'L03'},{17,'L02'},...
              {18,'L01'},{19,'Pin 1'},{20,'Pin 2'}, {21,'Pin 3'},{22,'Pin 4'},{23,'Pin 5'},...
              {24,'Pin 6'}};
      elseif EEG.nbchan==30
              channelnames={{1,'R08'},{2,'R07'},{3,'R06'}, {4,'R05'},{5,'R04'},...
              {6,'R03'},{7,'R02'},{8,'R01'}, {9,'L08'},{10,'L07'},{11,'L06'},...
              {12,'L05'},{13,'L04b'},{14,'L04a'}, {15,'L04'},{16,'L03'},{17,'L02'},...
              {18,'L01'},{19,'Pin 1'},{20,'Pin 2'}, {21,'Pin 3'},{22,'Pin 4'},{23,'Pin 5'},...
              {24,'Pin 6'},{25,'Yaw'},{26,'Pitch'}, {27,'Roll'},{28,'AccelerX'},{29,'AccelerY'}, {30,'AccelerZ'}} ;
               
     else
         channelnames={{1,'R08'},{2,'R07'},{3,'R06'}, {4,'R05'},{5,'R04'},...
              {6,'R03'},{7,'R02'},{8,'R01'}, {9,'L08'},{10,'L07'},{11,'L06'},...
              {12,'L05'},{13,'L04b'},{14,'L04a'}, {15,'L04'},{16,'L03'},{17,'L02'},...
              {18,'L01'}};
     end
     
     
         case 'smarting v35 transparent'
    display('Two grids. Amp light facing experimenter. Pins: Bottom right.')
     if EEG.nbchan==27
      channelnames={{1,'R08'},{2,'R07'},{3,'R06'}, {4,'R05'},{5,'R04'},...
              {6,'R03'},{7,'R02'},{8,'R01'}, {9,'L08'},{10,'L07'},{11,'L06'},...
              {12,'L05'},{13,'L04b'},{14,'L04a'}, {15,'L04'},{16,'L03'},{17,'L02'},...
              {18,'L01'},{19,'R_Concha_unten'},{20,'R_Concha_vorne'}, {21,'R_Concha_oben'},{22,'L_Concha_oben'},{23,'L_Concha_vorne'},...
              {24,'L_Concha_unten'},{25,'Yaw'},{26,'Pitch'}, {27,'Roll'}};
     elseif  EEG.nbchan==24    
         channelnames={{1,'R08'},{2,'R07'},{3,'R06'}, {4,'R05'},{5,'R04'},...
              {6,'R03'},{7,'R02'},{8,'R01'}, {9,'L08'},{10,'L07'},{11,'L06'},...
              {12,'L05'},{13,'L04b'},{14,'L04a'}, {15,'L04'},{16,'L03'},{17,'L02'},...
              {18,'L01'},{19,'R_Concha_unten'},{20,'R_Concha_vorne'}, {21,'R_Concha_oben'},{22,'L_Concha_oben'},{23,'L_Concha_vorne'},...
              {24,'L_Concha_unten'}};
      
         
         
     else
         msg = ['Error occurred. There are ' num2str(EEG.nbchan) ' channels in your data. I cannot deal with this, you should have either 24 or 27 channels. As you do not you either do not know what you are doing or you know it exactly what you are doing. In the first case: Get some help. In the second case: deal with it, I cannot help you.' ];
         error(msg)
     end
       
    case 'cEEGrid Z1'
        display('Stefans Setup.')
            channelnames={{1,'R07'},{2,'R03'},{3,'R01'}, {4,'Second Pin (fl)'},{5,'Thrid Pin (fl)'},...
              {6,'Forth Pin (fl)'}, {7,'First Pin (fl)'},{8,'R02'},{9,'R04'},...
              {10,'R08'},{11,'R010'},{12,'R9'}, {13,'L9'},{14,'L07'},{15,'L04a'},...
              {16,'L03'},{17,'L01'},{18,'L02'}, {19,'L04'},{20,'L04b'},{21,'L08'},...
              {22,'L010'},{23,'Yaw'},{24,'Pitch'}, {25,'Roll'}};
    

    case 'smartphone razor'
              
        channelnames={{1,'R01'},{2,'R02'},{3,'R03'}, {4,'R04'},{5,'R05'},...
              {6,'R06'},{7,'R07'},{8,'R08'}, {9,'L01'},{10,'L02'},{11,'L03'},...
              {12,'L04'},{13,'L04a'},{14,'L04b'}, {15,'L05'},{16,'L06'},{17,'L07'},...
              {18,'L08'}};
          
        types={{1,'EEG'},{2,'EEG'},{3,'EEG'}, {4,'EEG'},{5,'EEG'},...
              {6,'EEG'},{7,'EEG'},{8,'EEG'}, {9,'EEG'},{10,'EEG'},{11,'EEG'},...
              {12,'EEG'},{13,'EEG'},{14,'EEG'}, {15,'EEG'},{16,'EEG'},{17,'EEG'},...
              {18,'EEG'}};
        
    case 'tmsi'
        
              channelnames={{1,'R010'},{2,'R9'},{3,'R08'}, {4,'R07'},{5,'R06'},...
              {6,'R05'},{7,'R04'},{8,'R03'}, {9,'R02'},{10,'R01'},{11,'L010'},...
              {12,'L9'},{13,'L08'},{14,'L07'}, {15,'L06'},{16,'L05'},{17,'L04'},...
              {18,'L03'},{19,'L02'},{20,'L01'}, {21,'x1'},{22,'x2'},{23,'x3'},...
              {24,'x4'},{25,'x5'},{26,'x6'}, {27,'x1'}};
 
              EEG = pop_select( EEG,'channel',[1:20]);

    
     end


        for ch=1:numel(channelnames)
              EEG.chanlocs(ch).labels=channelnames{ch}{2};
        end
        
        if  exist('types','var')
            for ch=1:numel(channelnames)
                  EEG.types(ch).type=types{ch}{2};
            end
        end
        % delete non EEG channels
        %if deletechannels
        %EEG = pop_select( EEG,'nochannel',{'x1' 'x2' 'x3' 'x4' 'x5' 'x6' 'GyroX' 'GyroY' 'GyroZ'});
        %end

end



