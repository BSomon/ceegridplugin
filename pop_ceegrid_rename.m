% pop_ceegrid_rename() - adds the correct labels for your ceegrid recording 
%		         depending on your connector layout. 
%
% Usage:
% >> OUTEEG =pop_ceegrid_rename(INEEG, AMP, addChannelLocations);
% 
% Inputs: 
%   INEEG     	  - input EEG dataset
%   AMP           - your recording setup 
%   addChannelLocations - whether you want to have channels
%
% Outputs: 
%   OUTEEG        - output dataset
%
% See also:
%    EEGLAB
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG, com] = pop_ceegrid_rename(EEG, AMP,addChannelLocations );

% the command output is a hidden output that does not have to
% be described in the header

com = ''; % this initialization ensure that the function will return something
          % if the user press the cancel button  

% display help if not enough arguments
% ------------------------------------
%if nargin < 1
%	help pop_ceegrid_rename;
%	return;
%end;

% pop up window
% -------------
possibleConfigurations = {'smarting v27',   'smarting v35', 'smarting v35 transparent','cEEGrid Z1','tmsi','Smartphone Razor'};

  %res = inputgui( 'geometry', { [1 1] }, ...
  %       'geomvert', [5], 'uilist', { ...
  %       { 'style', 'text', 'string', [ 'Select Setup' 10 10 10 ] }, ...
  %       { 'style', 'listbox', 'string', cell2mat(strcat(possibleAmplifiers,'|')) } } ); %convert list of options into correct format
  
  if nargin < 2
  res = inputgui( 'geometry', { [1 1 ] [1 1 ]}, ...
        'geomvert', [3 1],... 
        'uilist', { ...
        { 'style', 'text', 'string', [ 'Select Recording Setup' 10 10 10 ] }, ...
        { 'style', 'listbox', 'string', cell2mat(strcat(possibleConfigurations,'|')) },...
        {  },...
        { 'style', 'checkbox', 'string', 'Add channel locations'},...
        } );
     
    % call function
    AMP=possibleConfigurations{res{1}};
    addChannelLocations=res{2};
end

EEG=ceegrid_rename(EEG,AMP);

% add channels if requested
if addChannelLocations
    display('Adding channel locations');
    scriptName = mfilename('fullpath');
    [currentpath, filename, fileextension]= fileparts(scriptName);
    channelLocationFile=[currentpath '\elec_cEEGrid.elp'];
    EEG=pop_chanedit(EEG, 'lookup',channelLocationFile);
end


% return the string command
% -------------------------
%com = sprintf('pop_ceegrid_rename( %s, %d, [%s] );', inputname(1), AMP, int2str(param3));

return;