# Ear-EEG at home

In the .zip file you find three EEGLAB files. 
Each file contains one hour of ear-EEG data, collected with the cEEGrid and the
nEEGlace. The data was recorded while I was sitting at my desk, having skype 
meetings, and walking up and down the stairs. In this version there is no additional
information about what I did at each moment. 

There is some channel positioning information in the dataset. 
You can use the cEEGrrid EEGlab plugin for some basic visualisation:
(https://gitlab.uni-oldenburg.de/erer5685/ceegridplugin)

The data was preprocessed using ASR (clean_rawdata plugin in EEGlab) and filtered
between 0.01 and 45 Hz. 

There is no additional event information in the data. 
There are merely two events in the dataset "start" and "stop", roughly one minute 
apart. During this minute I kept my eyes closed. For this period there is a clear 
peak in the power spectrum around 10 Hz. 


*  EEG_at_home_20200324_1.set collected betwen 9:00 and 10:00 o'clock.
* EEG_at_home_20200324_2.set collected betwen 10:00 and 11:00 o'clock.
* EEG_at_home_20200324_3.set collected betwen 11:00 and 12:00 o'clock.`

More information will follow.

You can play with the data. Just let me know what you found out. 

### What are cEEGrids? 
The cEEGrid is an electrode array for measuring EEG scalp potentials around the 
ear. It was developed in the Neuropschology Lab in the Psychology Department of
the Carl von Ossietzky University Oldenburg, Germany.

More details on how to use it, can be found here: http://ceegrid.com/home/