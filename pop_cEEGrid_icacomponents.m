% pop_cEEGrid_icacomponents(INEEG, nargin); - uses fieldtrip functions to plot data in the cEEGrid shape.
%
% Usage:
% >> OUTEEG =pop_cEEGrid_icacomponents(INEEG, nargin);
%
% Inputs:
%   INEEG         - input EEG dataset
%   AMP           - your recording setup
%
% Outputs:
%   OUTEEG        - output dataset
%
% See also:
%    EEGLAB
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG, com] = pop_cEEGrid_icacomponents(EEG, layoutname);

com = ''; % this initialization ensure that the function will return something

if nargin < 1
	help pop_cEEGrid_icacomponents;
	return;
end;	

% get all layouts from the layouts directory
scriptName = mfilename('fullpath');
[currentpath, filename, fileextension]= fileparts(scriptName);
layouts=dir([currentpath filesep 'Layouts',filesep '*.mat']);
layoutoptions=['']
for k=1:numel(layouts)
    layoutoptions=strcat(layoutoptions,layouts(k).name(1:end-4));
    if k<numel(layouts)
        layoutoptions=strcat(layoutoptions,' | ');
    end
end


if nargin < 2    
        guititle='cEEGrid ICA components';
        geom={1 };
        uilist={{ 'style', 'text', 'string', 'Plot topographies and time courses' }};
        result=inputgui('geometry', geom , 'uilist', uilist);
        %uses both sides by default
       cfg.side = layouts(1).name(1:end-4); % get the layout file
else
    cfg.side = layoutname;
end

        
        for comp=1:EEG.nbchan
                    dimVec=EEG.times;
                    
                    Data=mean(EEG.icaact(comp,:,:),3);
                    DataLabels={EEG.chanlocs.labels};
                    figure
                    subplot(2,1,1)
                    plot(Data);
                    title(['Component ' num2str(comp)]);
                    subplot(2,1,2)              
                    cfg.channel={'all' };
                    cfg.interplimits ='head'
                    cfg.interpolation='nearest'
                    cfg.plotSort='ft_topoplotER';
                    cfg.newfig='no';
                    dimVec=1;
                    Data=EEG.icawinv(:,comp);
                    DataLabels={EEG.chanlocs.labels};
                    ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg);
                end

com = sprintf('pop_cEEGrid_icacomponents( %s, %s);', inputname(1), vararg2str({cfg.side}));

end