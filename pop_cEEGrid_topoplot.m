% pop_cEEGrid_topoplot(INEEG, nargin); - uses fieldtrip functions to plot data in the cEEGrid shape.
%
% Usage:
% >> OUTEEG =pop_cEEGrid_topoplot(INEEG, nargin);
%
% Inputs:
%   INEEG         - input EEG dataset
%   AMP           - your recording setup
%
% Outputs:
%   OUTEEG        - output dataset
%
% See also:
%    EEGLAB
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG, com] = pop_cEEGrid_topoplot(EEG, layoutname, timepoints);

com = ''; % this initialization ensure that the function will return something

if nargin < 1
	help pop_cEEGrid_topoplot;
	return;
end;	

% get all layouts from the layouts directory
scriptName = mfilename('fullpath');
[currentpath, filename, fileextension]= fileparts(scriptName);
layouts=dir([currentpath filesep 'Layouts',filesep '*.mat']);
layoutoptions=['']
for k=1:numel(layouts)
    layoutoptions=strcat(layoutoptions,layouts(k).name(1:end-4));
    if k<numel(layouts)
        layoutoptions=strcat(layoutoptions,' | ');
    end
end

if nargin < 2
 guititle='cEEGrid topoplotER -- pop_cEEGrid_topoplot';
        geom={[1.5 1]  [1.5 1] 1 [1.5 1] [1 1 ] [1] [1]};
        
        txtwhat2plot1 = 'Plotting ERP scalp maps at this latency';
        txtwhat2plot2 = sprintf('(range: %d to %d ms, two values -> mean over time range):', ...
            round(EEG.xmin*1000), round(EEG.xmax*1000));
        editwhat2plot = [''];
        editwhichchannels = [''];
        editoptions=[''];
        uilist={{ 'style', 'text', 'string', 'Channels subset([]=all)' },...
            { 'style', 'edit', 'string', editwhichchannels } ,...
            {'style', 'text', 'string', txtwhat2plot1},...
            { 'style', 'edit', 'string', editwhat2plot},...
            {'style', 'text', 'string', txtwhat2plot2},...
            { 'style'   'text'     'string'    'Plot title' } ...
            { 'style'   'edit'     'string'    fastif(~isempty(EEG.setname), [EEG.setname], '') } ...
            { 'style', 'text', 'string', 'Select Grid:' },...
            { 'style', 'listbox', 'string', layoutoptions},...
            { 'style'   'text'     'string'    [ '-> Additional plot options (see Help)' ] } ...
            { 'style'   'edit'     'string'  editoptions },...
            };

                result=inputgui( geom ,  uilist, 'pophelp(''pop_cEEGrid_topoplot'')', guititle, [], 'normal');


      cfg.side = layouts((result{4})).name(1:end-4); % get the layout file
      timepoints=str2num(result{2});
                
                
    if isempty(result{1})
        DataLabels={EEG.chanlocs.labels};
    else
        DataLabels=strsplit(result{1});
    end
else
    

  
    cfg.side = layoutname;
    DataLabels={EEG.chanlocs.labels};

end
    
    [x,cindex,z] = closest(EEG.times, timepoints);
    if numel(cindex)==1
        Data=mean(mean(EEG.data(:,cindex(1):cindex(1),:),3),2);
    else
        Data=mean(mean(EEG.data(:,cindex(1):cindex(2),:),3),2);
    end
    
    cfg.channel={'all'};
    cfg.interplimits ='head';
    cfg.interpolation='nearest';
    cfg.plotSort='ft_topoplotER';
     dimVec=1;
    ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg)


% return the string command
% -------------------------
com = sprintf('pop_cEEGrid_topoplot( %s, %s);', inputname(1), vararg2str({cfg.side,timepoints}));

return;