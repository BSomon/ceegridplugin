% eegplugin_ceegrid() - A plugin for plotting cEEGrid data using fieltrip functions. 
% 
% Usage:
%   >> eegplugin_ceegrid(fig,try_strings, catch_strings); 
%
%   
% Author: Martin Georg Bleichner, Neurophysiology of everyday life lab , University of Oldenburg 2019
%		  Bertille Somon, Artificial and Natural Intelligence Toulouse Institute - ISAE Supaero, Université de Toulouse, France 2020
% History:
% 29/05/2020 ver 0.6 by Bertille adding the Study functions
% 17/10/2018 ver 0.40 by Martin 
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function vers= eegplugin_ceegrid(fig,try_strings, catch_strings)
    
vers = 'ceegrid_0.6';

% Check for fieldtrip dependencies
if ~plugin_status('fieldtrip-lite')
    display('The fieldtrip-lite plugin is not installed. Please install the fieldtrip-lite plugin manually.'); 
    % this does not work
    %plugin_askinstall('fieldtrip-lite', [], true); 
end
    

mu= findobj(fig, 'tag', 'tools');

cmd =[try_strings.no_check '[EEG LASTCOM] = pop_ceegrid_rename(EEG);'];
cmd=[cmd '[ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);' catch_strings.add_to_hist];

finalcmd=cmd;
uimenu( mu, 'label', 'cEEGrid rename', 'callback', finalcmd);

mu= findobj(fig, 'tag', 'plot');

uimenu(mu, 'label', 'cEEGrid channel ERP', 'callback', [try_strings.no_check '[EEG LASTCOM] = pop_cEEGrid_multiplot(EEG); [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG,EEG, CURRENTSET);' catch_strings.add_to_hist], 'userdata','chanloc:on');
uimenu(mu, 'label', 'cEEGrid topography', 'callback', [try_strings.no_check  '[EEG LASTCOM] = pop_cEEGrid_topoplot(EEG); [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG,EEG, CURRENTSET);'  catch_strings.add_to_hist], 'userdata','chanloc:on');
uimenu(mu, 'label', 'cEEGrid ICA topography', 'callback', [try_strings.check_ica '[EEG LASTCOM] = pop_cEEGrid_icatopoplot(EEG);[ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG,EEG, CURRENTSET);'  catch_strings.store_and_hist ], 'userdata','chanloc:on');
uimenu(mu, 'label', 'cEEGrid ICA components', 'callback', [try_strings.check_ica '[EEG LASTCOM] = pop_cEEGrid_icacomponents(EEG);[ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG,EEG, CURRENTSET);' catch_strings.store_and_hist  ], 'userdata','chanloc:on');

% Added for plotting in the study - BS
mu = findobj(fig, 'tag', 'study');
uimenu(mu, 'label', 'Plotting cEEGrid', 'callback', [try_strings.no_check '[STUDY, ALLEEG, LASTCOM] = pop_ceegrid_advanced_plotting_std(STUDY, ALLEEG); STUDY = std_checkset(STUDY, ALLEEG);' catch_strings.add_to_hist], 'userdata','study:on');
