% pop_cEEGrid_multiplot(EEG, nargin); - uses fieldtrip functions to plot data in the cEEGrid shape.
%
% Usage:
% >> OUTEEG =pop_ceegrid_basic_plotting(EEG, nargin);
%
% Inputs:
%   EEG         - input EEG dataset
%   layoutname      - choose from different layouts
% Outputs:
%   OUTEEG        - output dataset
%
% See also:
%    EEGLAB
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG, com] = pop_cEEGrid_multiplot(EEG, layoutname);

com = ''; % this initialization ensure that the function will return something

if nargin < 1
    help pop_cEEGrid_multiplot;
    return;
end

% get all layouts from the layouts directory
scriptName = mfilename('fullpath');
[currentpath, filename, fileextension]= fileparts(scriptName);
layouts=dir([currentpath filesep 'Layouts',filesep '*.mat']);
layoutoptions=[''];
for k=1:numel(layouts)
    layoutoptions=strcat(layoutoptions,layouts(k).name(1:end-4));
    if k<numel(layouts)
        layoutoptions=strcat(layoutoptions,' | ');
    end
end

if nargin < 2
    
    guititle='cEEGrid multiplot-- pop_cEEGrid_multiplot';
    geom={[1 1] [1 1] [1 1]  [1] [1]};
    
    
    editoptions=[''];
    editwhichchannels = [''];
    
    uilist={
        { 'style', 'text', 'string', 'Channels subset([]=all) ' },...
        { 'style', 'edit', 'string', editwhichchannels },...
        { 'style'   'text'     'string'    'Plot title' } ...
        { 'style'   'edit'     'string'    fastif(~isempty(EEG.setname), [EEG.setname], '') } ...
        { 'style', 'text', 'string', 'Select Grid:' },...
        { 'style', 'listbox', 'string', layoutoptions},...
        { 'style'   'text'     'string'    [ '-> Additional plot options (see Help)' ] } ...
        { 'style'   'edit'     'string'  editoptions }};
    
    result=inputgui( geom ,  uilist, 'pophelp(''pop_cEEGrid_multiplot'')', guititle, [], 'normal');
    
    if isempty(result), return; end
    
    
   
    
    
    % if no config file is given make empty config file
    %if nargin < 3
    %    cfg = [];
    %end
    
    
    if result{3}
        % set layout file
        cfg.side = layouts((result{3})).name(1:end-4); % get the layout file
    end
    
    if isempty(result{1})
        DataLabels={EEG.chanlocs.labels};
    else
        DataLabels=strsplit(result{1});
    end
else
     cfg.side = layoutname;
     DataLabels={EEG.chanlocs.labels};
      
end

dimVec = EEG.times;
Data = mean(EEG.data,3);
% use either all channels or the ones specified

ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg);

% return the string command
% -------------------------
com = sprintf('pop_cEEGrid_multiplot( %s, %s );', inputname(1), vararg2str(cfg.side));

return;